import router from '../router'

export default {
  user: {
    authenticated: false
  },

  login (context, creds, redirect) {
    let self = this

    context.$http.post(context.$root.config.loginUrl, creds)
      .then(function (response) {
        localStorage.setItem('id_token', response.data.token)
        localStorage.setItem('iam', JSON.stringify(response.data.user))
        localStorage.setItem('userRoles', JSON.stringify(response.data.userRoles))
        localStorage.setItem('currency', JSON.stringify(response.data.currency))
        localStorage.setItem('agency', JSON.stringify(response.data.agency))
        if (response.data.streets && response.data.districts) {
          localStorage.setItem('streets', JSON.stringify(response.data.streets))
          localStorage.setItem('districts', JSON.stringify(response.data.districts))
        }

        self.user.authenticated = true
        if (redirect) {
          router.push(redirect)
        }
      }, function (response) {
        context.error = response
        context.formSubmitted = false

        let strErrors = ''
        if (context.error.data && context.error.data.error) {
          context.error.data.error.errors.forEach(a => {
            if (a[0].indexOf('email') !== -1) {
              strErrors += context.$text('emailRequired') + '<br />'
            }
            if (a[0].indexOf('password') !== -1) {
              strErrors += context.$text('passwordRequired') + '<br />'
            }
          })
          context.$toastr('error', strErrors, context.$text('toastr_error_header'))
        }
      })
  },

  register (context, creds, redirect) {
    // let self = this
    context.$http.post(context.$root.config.registerUrl, creds)
      .then(function (response) {
        context.formSubmitted = false
        if (redirect && response.data.success) {
          context.$toastr(
            'success',
            context.$text(response.data.success),
            context.$text('toastr_success_header')
          )
          router.push(redirect)
        }
      }, function (response) {
        context.$toastr('error', context.$text(response.data.error), context.$text('toastr_error_header'))
        context.error = response
        context.formSubmitted = false
      })
  },

  logout () {
    localStorage.removeItem('id_token')
    localStorage.removeItem('iam')
    localStorage.removeItem('userRoles')
    localStorage.removeItem('streets')
    localStorage.removeItem('districts')
    localStorage.removeItem('currency')
    localStorage.removeItem('agency')
    this.user.authenticated = false
    router.push('/login')
  },

  checkLogin () {
    let token = localStorage.getItem('id_token')
    if (token) {
      this.user.authenticated = true
    }
  },
}
