import $ from 'jquery'
import iMask from '../utils/inputmask'

iMask.install($)

export default (Vue) => {
  Vue.directive('input-mask', {
    bind (el, binding) {
      $(el).inputmask({
        mask: binding.value,
        placeholder: 'x',
      })
    },
  })
}
