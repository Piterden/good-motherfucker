export default (Vue) => {
  Vue.directive('select-streets', {
    bind (el, binding) {
      let $el = $(el)

      if (binding.arg.length) {
        $el.SumoSelect({
          placeholder: el.dataset.placeholder,
          data: binding.arg,
          search: true,
          searchText: 'Искать',
          captionFormat: '{0} Выбрано',
          noMatch: 'Такой улицы не найдено',
          searchOnInit: true,
        })

        $el.on('change', (e) => {
          el.dataset.val = e.target.value
        })
      }
    },

    unbind (el) {
      let $el = $(el)

      if (typeof $el.eq(0).sumo !== 'undefined') {
        $el.eq(0).sumo.unload()
      }
    },
  })
}
