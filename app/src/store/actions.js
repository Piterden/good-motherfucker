import { SET_LOADING, TOKEN_UPDATE, USER_UPDATE } from './mutation-types'

export default {
  SET_LOADING ({ commit }, value) {
    commit(SET_LOADING, value)
  },

  TOKEN_UPDATE ({ commit }, token) {
    commit(TOKEN_UPDATE, token)
  },

  USER_UPDATE ({ commit }, user) {
    commit(USER_UPDATE, user)
  },
}
