export const
  getLoading = (state) => {
    let status = false
    Object.keys(state).forEach((module) => {
      if (module.isLoaded) {
        return
      }
      status = true
    })
    return status
  },
  isGuest = (state) => Number(state.user.user.id) === 0,
  getToken = (state) => state.token,
  getUser = (state) => state.user
