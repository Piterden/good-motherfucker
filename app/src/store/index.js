import Vue from 'vue'
import Vuex from 'vuex'
import actions from './actions'
import mutations from './mutations'
import * as getters from './getters'

import cities from './modules/cities'
import shares from './modules/shares'
import streets from './modules/streets'
import objects from './modules/objects'
import reports from './modules/reports'
import agencies from './modules/agencies'
import districts from './modules/districts'
import telegramUsers from './modules/telegram_users'
import telegramFilters from './modules/telegram_filters'

Vue.use(Vuex)

export default {
  state: {
    loading: false,
    token: null,
    user: {
      id: 0,
    },
  },

  getters: getters,
  actions: actions,
  mutations: mutations,

  modules: {
    cities,
    shares,
    streets,
    objects,
    reports,
    agencies,
    districts,
    telegramUsers,
    telegramFilters,
  },
}
