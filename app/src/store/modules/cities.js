import Vue from 'vue'
import {
  CITY_LOAD,
  CITY_UNLOAD,
  CITY_UPDATE,
  CITIES_PUSH,
  CITIES_PULL,
  CITIES_UPDATE,
} from '../mutation-types'

const
  state = {
    isLoaded: false,
    cities: [],
    city: null,
  },

  mutations = {
    [CITY_LOAD] (state, city) {
      Vue.set(state, 'city', city)
    },

    [CITY_UNLOAD] (state) {
      Vue.set(state, 'city', null)
    },

    [CITY_UPDATE] (state, city) {
      let idx = state.cities.findIndex(item => city.id === item.id)
      if (idx >= 0) {
        Vue.set(state.cities, idx, city)
        if (state.city && state.city.id === city.id) {
          Vue.set(state, 'city', city)
        }
      } else {
        Vue.set(state.cities, state.cities.length, city)
      }
    },

    [CITIES_PUSH] (state, city) {
      Vue.set(state.cities, state.cities.length, city)
    },

    [CITIES_PULL] (state, city) {
      state.cities.splice(state.cities.findIndex(item => city.id === item.id), 1)
      if (state.city && state.city.id === city.id) {
        Vue.set(state, 'city', null)
      }
    },

    [CITIES_UPDATE] (state, cities) {
      Vue.set(state, 'cities', cities)
      Vue.set(state, 'isLoaded', true)
    },
  },

  actions = {
    CITY_LOAD ({ commit }, city) {
      commit(CITY_LOAD, city)
    },
    CITY_UNLOAD ({ commit }) {
      commit(CITY_UNLOAD)
    },
    CITY_UPDATE ({ commit }, city) {
      commit(CITY_UPDATE, city)
    },
    CITIES_PUSH ({ commit }, city) {
      commit(CITIES_PUSH, city)
    },
    CITIES_PULL ({ commit }, city) {
      commit(CITIES_PULL, city)
    },
    CITIES_UPDATE ({ commit }, cities) {
      commit(CITIES_UPDATE, cities)
    },
  },

  getters = {
    citiesLoaded: state => state.cities.isLoaded,
    getCities: state => state.cities.cities,
    getCity: state => state.cities.city,
  }

export default {
  state,
  actions,
  getters,
  mutations,
}
