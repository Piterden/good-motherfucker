import Vue from 'vue'
import {
  OBJECT_LOAD,
  OBJECT_UNLOAD,
  OBJECT_UPDATE,
  OBJECTS_PUSH,
  OBJECTS_PULL,
  OBJECTS_UPDATE,
} from '../mutation-types'

const
  state = {
    object: null,
    objects: [],
    isLoaded: false,
  },

  mutations = {
    [OBJECT_LOAD] (state, object) {
      Vue.set(state, 'object', object)
    },

    [OBJECT_UNLOAD] (state) {
      Vue.set(state, 'object', null)
    },

    [OBJECT_UPDATE] (state, object) {
      let idx = state.objects.findIndex(item => object.id === item.id)
      if (idx >= 0) {
        Vue.set(state.objects, idx, object)
        if (state.object && state.object.id === object.id) {
          Vue.set(state, 'object', object)
        }
      } else {
        Vue.set(state.objects, state.objects.length, object)
      }
    },

    [OBJECTS_PUSH] (state, object) {
      Vue.set(state.objects, state.objects.length, object)
    },

    [OBJECTS_PULL] (state, object) {
      state.objects.splice(state.objects.findIndex(item => object.id === item.id), 1)
      if (state.object && state.object.id === object.id) {
        Vue.set(state, 'object', null)
      }
    },

    [OBJECTS_UPDATE] (state, objects) {
      Vue.set(state, 'objects', objects)
      Vue.set(state, 'isLoaded', true)
    },
  },

  actions = {
    OBJECT_LOAD ({ commit }, object) {
      commit(OBJECT_LOAD, object)
    },
    OBJECT_UNLOAD ({ commit }) {
      commit(OBJECT_UNLOAD)
    },
    OBJECT_UPDATE ({ commit }, object) {
      commit(OBJECT_UPDATE, object)
    },
    OBJECTS_PUSH ({ commit }, object) {
      commit(OBJECTS_PUSH, object)
    },
    OBJECTS_PULL ({ commit }, object) {
      commit(OBJECTS_PULL, object)
    },
    OBJECTS_UPDATE ({ commit }, objects) {
      commit(OBJECTS_UPDATE, objects)
    },
  },

  getters = {
    objectsLoaded: state => state.objects.isLoaded,
    getObjects: state => state.objects.objects,
    getObject: state => state.objects.object,
  }

export default {
  state,
  actions,
  getters,
  mutations,
}
