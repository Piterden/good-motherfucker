import Vue from 'vue'
import { SET_LOADING, TOKEN_UPDATE, USER_UPDATE } from './mutation-types'

export default {
  [SET_LOADING] (state, value) {
    Vue.set(state, 'loading', value)
  },

  [TOKEN_UPDATE] (state, token) {
    Vue.set(state, 'token', token)
  },

  [USER_UPDATE] (state, user) {
    Vue.set(state, 'user', user)
  },
}
