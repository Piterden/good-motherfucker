exports.install = function ($) {
  let
    isIphone = (window.orientation !== undefined),
    isAndroid = navigator.userAgent.toLowerCase().indexOf('android') > -1,
    isIE = window.navigator.appName === 'Microsoft Internet Explorer',

    Inputmask = function (element, options) {
      if (isAndroid) {
        return
      }

      this.$element = $(element)
      this.options = $.extend({}, Inputmask.DEFAULTS, options)
      this.mask = String(this.options.mask)

      this.init()
      this.listen()
      this.checkVal()
    },

    old

  Inputmask.DEFAULTS = {
    mask: '',
    placeholder: '_',
    definitions: {
      9: '[0-9]',
      a: '[A-Za-z]',
      w: '[A-Za-z0-9]',
      '*': '.'
    }
  }

  Inputmask.prototype.init = function () {
    let
      defs = this.options.definitions,
      len = this.mask.length

    this.tests = []
    this.partialPosition = this.mask.length
    this.firstNonMaskPos = null

    $.each(this.mask.split(''), $.proxy(function (i, c) {
      if (c === '?' && len) {
        --len
        this.partialPosition = i
      } else if (defs[c]) {
        this.tests.push(new RegExp(defs[c]))
        if (this.firstNonMaskPos === null) {
          this.firstNonMaskPos = this.tests.length - 1
        }
      } else {
        this.tests.push(null)
      }
    }, this))

    this.buffer = $.map(this.mask.split(''), $.proxy(function (c, i) {
      if (c !== '?') return defs[c] ? this.options.placeholder : c
    }, this))

    this.focusText = this.$element.val()

    this.$element.data('rawMaskFn', $.proxy(function () {
      return $.map(this.buffer, function (c, i) {
        return this.tests[i] && c !== this.options.placeholder ? c : null
      }).join('')
    }, this))
  }

  Inputmask.prototype.listen = function () {
    if (this.$element.attr('readonly')) {
      return
    }

    let pasteEventName = (isIE ? 'paste' : 'input') + '.bs.inputmask'

    this.$element
      .on('unmask.bs.inputmask', $.proxy(this.unmask, this))
      .on('focus.bs.inputmask', $.proxy(this.focusEvent, this))
      .on('blur.bs.inputmask', $.proxy(this.blurEvent, this))
      .on('keydown.bs.inputmask', $.proxy(this.keydownEvent, this))
      .on('keypress.bs.inputmask', $.proxy(this.keypressEvent, this))
      .on(pasteEventName, $.proxy(this.pasteEvent, this))
  }

  /**
   * Helper Function for Caret positioning
   *
   * @param      {number}  begin   The begin
   * @param      {number}  end     The end
   * @return     {Object}  { description_of_the_return_value }
   */
  Inputmask.prototype.caret = function (begin, end) {
    if (this.$element.length === 0) return
    if (typeof begin === 'number') {
      end = (typeof end === 'number') ? end : begin
      return this.$element.each(function () {
        if (this.setSelectionRange) {
          this.setSelectionRange(begin, end)
        } else if (this.createTextRange) {
          let range = this.createTextRange()
          range.collapse(true)
          range.moveEnd('character', end)
          range.moveStart('character', begin)
          range.select()
        }
      })
    } else {
      if (this.$element[0].setSelectionRange) {
        begin = this.$element[0].selectionStart
        end = this.$element[0].selectionEnd
      } else if (document.selection && document.selection.createRange) {
        let range = document.selection.createRange()
        begin = 0 - range.duplicate().moveStart('character', -100000)
        end = begin + range.text.length
      }
      return {
        begin: begin,
        end: end
      }
    }
  }

  Inputmask.prototype.seekNext = function (pos) {
    let len = this.mask.length
    while (++pos <= len && !this.tests[pos]);

    return pos
  }

  Inputmask.prototype.seekPrev = function (pos) {
    while (--pos >= 0 && !this.tests[pos]);

    return pos
  }

  Inputmask.prototype.shiftL = function (begin, end) {
    let len = this.mask.length

    if (begin < 0) return

    for (let i = begin, j = this.seekNext(end); i < len; i++) {
      if (this.tests[i]) {
        if (j < len && this.tests[i].test(this.buffer[j])) {
          this.buffer[i] = this.buffer[j]
          this.buffer[j] = this.options.placeholder
        } else {
          break
        }
        j = this.seekNext(j)
      }
    }
    this.writeBuffer()
    this.caret(Math.max(this.firstNonMaskPos, begin))
  }

  Inputmask.prototype.shiftR = function (pos) {
    let len = this.mask.length

    for (let i = pos, c = this.options.placeholder; i < len; i++) {
      if (this.tests[i]) {
        let
          j = this.seekNext(i),
          t = this.buffer[i]
        this.buffer[i] = c
        if (j < len && this.tests[j].test(t)) {
          c = t
        } else {
          break
        }
      }
    }
  }

  Inputmask.prototype.unmask = function () {
    this.$element
      .unbind('.bs.inputmask')
      .removeData('bs.inputmask')
  }

  Inputmask.prototype.focusEvent = function () {
    this.focusText = this.$element.val()

    let
      len = this.mask.length,
      pos = this.checkVal(),
      that,
      moveCaret

    this.writeBuffer()

    that = this
    moveCaret = function () {
      if (pos === len) {
        that.caret(0, pos)
      } else {
        that.caret(pos)
      }
    }

    moveCaret()
    setTimeout(moveCaret, 50)
  }

  Inputmask.prototype.blurEvent = function () {
    this.checkVal()
    if (this.$element.val() !== this.focusText) {
      this.$element.trigger('change')
      this.$element.trigger('input')
    }
  }

  Inputmask.prototype.keydownEvent = function (e) {
    let k = e.which

    // backspace, delete, and escape get special treatment
    if (k === 8 || k === 46 || (isIphone && k === 127)) {
      let
        pos = this.caret(),
        begin = pos.begin,
        end = pos.end

      if (end - begin === 0) {
        begin = k !== 46 ? this.seekPrev(begin) : (end = this.seekNext(begin - 1))
        end = k === 46 ? this.seekNext(end) : end
      }

      this.clearBuffer(begin, end)
      this.shiftL(begin, end - 1)

      return false
    } else if (k === 27) {
      this.$element.val(this.focusText)
      this.caret(0, this.checkVal())

      return false
    }
  }

  Inputmask.prototype.keypressEvent = function (e) {
    let
      len = this.mask.length,
      k = e.which,
      pos = this.caret()

    if (e.ctrlKey || e.altKey || e.metaKey || k < 32) {
      return true
    } else if (k) {
      if (pos.end - pos.begin !== 0) {
        this.clearBuffer(pos.begin, pos.end)
        this.shiftL(pos.begin, pos.end - 1)
      }

      let p = this.seekNext(pos.begin - 1)

      if (p < len) {
        let c = String.fromCharCode(k)

        if (this.tests[p].test(c)) {
          this.shiftR(p)
          this.buffer[p] = c
          this.writeBuffer()
          let next = this.seekNext(p)
          this.caret(next)
        }
      }

      return false
    }
  }

  Inputmask.prototype.pasteEvent = function () {
    let that = this

    setTimeout(function () {
      that.caret(that.checkVal(true))
    }, 0)
  }

  Inputmask.prototype.clearBuffer = function (start, end) {
    let len = this.mask.length

    for (let i = start; i < end && i < len; i++) {
      if (this.tests[i]) {
        this.buffer[i] = this.options.placeholder
      }
    }
  }

  Inputmask.prototype.writeBuffer = function () {
    return this.$element.val(this.buffer.join('')).val()
  }

  Inputmask.prototype.checkVal = function (allow) {
    let
      len = this.mask.length,
      test = this.$element.val(),
      lastMatch = -1

    for (let i = 0, pos = 0; i < len; i++) {
      if (this.tests[i]) {
        this.buffer[i] = this.options.placeholder
        while (pos++ < test.length) {
          let c = test.charAt(pos - 1)
          if (this.tests[i].test(c)) {
            this.buffer[i] = c
            lastMatch = i
            break
          }
        }

        if (pos > test.length) {
          break
        }
      } else if (this.buffer[i] === test.charAt(pos) && i !== this.partialPosition) {
        pos++
        lastMatch = i
      }
    }

    if (!allow && lastMatch + 1 < this.partialPosition) {
      this.$element.val('')
      this.clearBuffer(0, len)
    } else if (allow || lastMatch + 1 >= this.partialPosition) {
      this.writeBuffer()
      if (!allow) {
        this.$element.val(this.$element.val().substring(0, lastMatch + 1))
      }
    }

    return (this.partialPosition ? lastMatch + 1 : this.firstNonMaskPos)
  }

  old = $.fn.inputmask

  $.fn.inputmask = function (options) {
    return this.each(function () {
      let
        $this = $(this),
        data = $this.data('bs.inputmask')

      if (!data) {
        $this.data('bs.inputmask', (data = new Inputmask(this, options)))
      }
    })
  }

  $.fn.inputmask.Constructor = Inputmask

  $.fn.inputmask.noConflict = function () {
    $.fn.inputmask = old
    return this
  }

  $(document).on('focus.bs.inputmask.data-api', '[data-mask]', function (e) {
    let $this = $(this)
    if ($this.data('bs.inputmask')) {
      return
    }
    $this.inputmask($this.data())
  })
}
