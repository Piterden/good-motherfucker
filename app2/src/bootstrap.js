import 'polyfill'
import 'asset/style/screen.scss'
import 'modernizr'
import 'settings'

import Vue from 'vue'
import Vuex from 'vuex'
import vMask from 'v-mask'
import VueRouter from 'vue-router'
import i18n from 'vue-i18n-manager'
import VueI18nUtils from 'vue-i18n-utils'

import App from './App'
import http from './util/http'
import router from './router'
import locales from './locales/index'
import eventbus from './util/eventbus'
import initStore from './store'

Vue.use(Vuex)
Vue.use(http)
Vue.use(vMask)
Vue.use(VueI18nUtils)
Vue.use(eventbus)
Vue.use(VueRouter)

Vue.config.silent = false
Vue.config.devTools = true

const store = initStore(Vue)

Vue.use(i18n, {
  store,
  config: {
    defaultCode: 'ru-RU',
    languages: [{
      name: 'Ukrainian',
      code: 'ua-UA',
      urlPrefix: 'ua',
      translationKey: 'ua',
    }, {
      name: 'Russian',
      code: 'ru-RU',
      urlPrefix: 'ru',
      translationKey: 'ru',
    }, {
      name: 'English',
      code: 'en-US',
      urlPrefix: 'en',
      translationKey: 'en',
    }],
    translations: locales,
  },
})

new Vue({
  ...App,
  el: '#app',
  router,
  store,
})

Vue.initI18nManager()
