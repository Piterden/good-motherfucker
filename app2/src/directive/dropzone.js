import Dropzone from 'dropzone'

export default (Vue) => {
  Vue.directive('filezone', {
    bind () {
      let
        vm = this,
        myDropzone = new Dropzone(this.el, {
          url: this.$root.config.apiUrl + '/files/object/' + this.params.object_id,
          headers: { 'Authorization': 'Bearer ' + localStorage.getItem('id_token') },
          acceptedFiles: '.jpg,.jpeg,.png,.gif',
          thumbnailWidth: 55,
          thumbnailHeight: 55,
          previewsContainer: this.el.children['dropzonePrev'],
          parallelUploads: 20,
          maxFiles: 20,
          previewTemplate: `
<div id="preview-template">
  <div class="dz-preview dz-file-preview">
    <div class="dz-details">
      <img data-dz-thumbnail/>
    </div>
    <div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress></span></div>
    <div class="dz-success-mark" data-dz-remove><span>✔</span></div>
    <div class="dz-error-mark" style="opacity:1" data-dz-remove><span>✘</span></div>
    <div class="dz-error-message"><span data-dz-errormessage></span></div>
  </div>
</div>`,
        })

      myDropzone.on('success', (file, response) => {
        file.fileName = response.fileName
      })

      myDropzone.on('sending', (file, xhr, formData) => {
        formData.append('fingerprint', vm.$finger())
      })

      myDropzone.on('removedfile', (file) => {
        let url = vm.$root.config.apiUrl + '/files/' + file.fileName
        vm.$http.delete(url)
          .then((response) => {})
          .catch((error) => {
            vm.error = error
          })
      })

      if (this.params.previews) {
        this.params.previews.forEach((item) => {
          let
            file = { width: 0, height: 0 },
            imageUrl = vm.$root.config.bucketObjectsUrl + '/' + item.name + '.jpg',
            mockFile = { name: 'Filename', fileName: item.name, size: 12345 },
            existingFileCount = 1

          myDropzone.emit('addedfile', mockFile)
          myDropzone.emit('thumbnail', mockFile, imageUrl)
          myDropzone.createThumbnailFromUrl(file, imageUrl, () => {}, 'Anonymous')
          myDropzone.emit('complete', mockFile)
          myDropzone.options.maxFiles = myDropzone.options.maxFiles - existingFileCount
          // myDropzone.createThumbnailFromUrl(file, imageUrl, function(){}, 'Anonymous')
        })
      }
    },
  })
}
