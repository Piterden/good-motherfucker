import agencies from './agencies.json'
import cities from './cities.json'
import districts from './districts.json'
import objectDatas from './object_datas.json'
import objects from './objects.json'
import previews from './previews.json'
import phones from './phones.json'
import streets from './streets.json'
import users from './users.json'

export default {
  agencies,
  cities,
  districts,
  objectDatas,
  objects,
  phones,
  previews,
  streets,
  users,
}
