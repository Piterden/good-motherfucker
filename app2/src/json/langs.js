import data from './'

let
  langs = ['ru', 'ua'],
  translations = ((langs) => {
    let out = {}
    langs.forEach((lang) => {
      out[lang] = {}
    })
    return out
  })(langs)

Object.keys(data).forEach((key) => {
  Object.keys(data[key]).forEach((field) => {
    langs.forEach((lang) => {
      if (field.endsWith('_' + lang)) {
        translations[lang][field.replace('_' + lang, '')] = data[key][field]
      }
    })
  })
})

export default translations
