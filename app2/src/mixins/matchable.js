export default {
  methods: {
    isMatch (path = '/') {
      let
        matched = this.$route.matched,
        result = false

      matched.forEach((route) => {
        if (route.path !== path &&
          route.path.startsWith(path)) {
          result = true
        }
      })

      return result
    },
  },
}
