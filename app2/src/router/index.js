import VueRouter from 'vue-router'

import routes from './routes'

console.log(process.env)

export default new VueRouter({
  base: '/',
  mode: 'history',
  routes: routes,
})
