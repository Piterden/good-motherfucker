import Vue from 'vue'
import {
  NOTIFICATION_LOAD,
  NOTIFICATION_UNLOAD,
  NOTIFICATION_UPDATE,
  NOTIFICATIONS_PUSH,
  NOTIFICATIONS_PULL,
  NOTIFICATIONS_UPDATE,
} from '../mutation-types'

const
  state = {
    notification: null,
    notifications: [],
    isLoaded: false,
  },

  mutations = {
    [NOTIFICATION_LOAD] (state, notification) {
      Vue.set(state, 'notification', notification)
    },

    [NOTIFICATION_UNLOAD] (state) {
      Vue.set(state, 'notification', null)
    },

    [NOTIFICATION_UPDATE] (state, notification) {
      let idx = state.notifications.findIndex(item => notification.id === item.id)
      if (idx > 0) {
        Vue.set(state.notifications, idx, notification)
        if (state.notification && state.notification.id === notification.id) {
          Vue.set(state, 'notification', notification)
        }
      } else {
        Vue.set(state.notifications, state.notifications.length, notification)
      }
    },

    [NOTIFICATIONS_PUSH] (state, notification) {
      Vue.set(state.notifications, state.notifications.length, notification)
    },

    [NOTIFICATIONS_PULL] (state, notification) {
      state.notifications.splice(state.notifications.findIndex(item => notification.id === item.id), 1)
      if (state.notification && state.notification.id === notification.id) {
        Vue.set(state, 'notification', null)
      }
    },

    [NOTIFICATIONS_UPDATE] (state, notifications) {
      Vue.set(state, 'notifications', notifications)
      Vue.set(state, 'isLoaded', true)
    },
  },

  actions = {
    NOTIFICATION_LOAD ({ commit }, notification) {
      commit(NOTIFICATION_LOAD, notification)
    },
    NOTIFICATION_UNLOAD ({ commit }) {
      commit(NOTIFICATION_UNLOAD)
    },
    NOTIFICATION_UPDATE ({ commit }, notification) {
      commit(NOTIFICATION_UPDATE, notification)
    },
    NOTIFICATIONS_PUSH ({ commit }, notification) {
      commit(NOTIFICATIONS_PUSH, notification)
    },
    NOTIFICATIONS_PULL ({ commit }, notification) {
      commit(NOTIFICATIONS_PULL, notification)
    },
    NOTIFICATIONS_UPDATE ({ commit }, notifications) {
      commit(NOTIFICATIONS_UPDATE, notifications)
    },
  },

  getters = {
    notificationsLoaded: state => state.isLoaded,
    getNotifications: state => state.notifications,
    getNotification: state => state.notification,
  }

export default {
  state,
  actions,
  getters,
  mutations,
}
