<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateObjects extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('objects', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('typehouse');
            $table->integer('type');
            $table->integer('category');
            $table->integer('subtype');
            $table->integer('rooms');
            $table->integer('user_id');
            $table->integer('square');

            $table->integer('square_area');
            $table->integer('view');
            $table->integer('floor');
            $table->integer('floors');
            $table->integer('appointment');

            $table->string('phone');
            $table->string('owner_name');

            $table->json('price_data');
            $table->json('duple_data');
            $table->json('prevs_data');

            $table->integer('city_id');
            $table->integer('district_id');
            $table->integer('agency_id');
            $table->integer('street_id');

            $table->string('notes_r');
            $table->string('notes_c');

            $table->float('rate');
            $table->tinyInteger('studio');
            $table->tinyInteger('status');
            $table->text('telegraph_url');
            $table->timestamp('block_ends_at');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('objects');
    }
}
